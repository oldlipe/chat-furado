<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<link href="css/estilo.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
	integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
	crossorigin="anonymous">
<link href="css/jquery-ui.min.css" rel="stylesheet">
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<title>Adiciona postagens</title>
</head>
<body>
	<form action="posta" method="POST">
		<div class="form-group">
			<label for="formGroupExampleInput">Username</label> 
			<input type="text" class="form-control" id="formGroupExampleInput"
				name="username" placeholder="Nickname"
				value="<c:out value='${message.username}' />" /> 
		</div>
		<div class="form-group">
			<label for="exampleFormControlInput1">Email</label> 
				<input type="email" class="form-control" 
				id="exampleFormControlInput1" name="email"
				placeholder="izuku@midorya.com"
				value="<c:out value='${message.email}' />" /> 
		</div>
		<div class="form-group">
				<label for="formGroupExampleInput">Mensagem</label> 
				<input type="text" class="form-control" 
				id="formGroupExampleInput" name="message"
				placeholder="Mensagem" value="<c:out value='${message.message}' />" />
		</div>
		<button type="submit" name="posta" 
		 class="btn btn-primary">Enviar</button>
				
	</form>
</body>



</html>