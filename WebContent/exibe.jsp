<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
	integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
	crossorigin="anonymous">
<meta charset="UTF-8">
<title>Postagens</title>
</head>
<body>
	<div class="jumbotron">
		<h1 class="display-4">Olá, participe do chat!</h1>
		<p class="lead">Simples chat para comunicações de diversos
			assuntos</p>
		<a class="btn btn-primary btn-lg" href="adiciona.jsp" role="button">Faça
			seu post agora!</a>
	</div>
	<div align="center">
		<table class="table table-sm">
			<tr>
				<th scope="col">ID</th>
				<th scope="col">Username</th>
				<th scope="col">Mensagem</th>
				<th scope="col">Email</th>
			</tr>
			<c:forEach var="message" items="${listaPosts}">
				<tr>
					<td><c:out value="${message.id}" /></td>
					<td><c:out value="${message.username}" /></td>
					<td><c:out value="${message.message}" /></td>
					<td><c:out value="${message.email}" /></td>
					<td><a
						href="query?logica=RemoveMessageController&id=${message.id}">
							Delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>