<%@ attribute name="mostra" required="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="contato" class="br.com.model.Message"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 
 
 <table border="1">
 		 <c:forEach var="contato" items="${contatos}" varStatus="id"> 
 			<tr bgcolor="#${id.count % 2 == 0 ? 'aaee88' : 'ffffff' }">
				<td>${contato.nome}</td>
				  <td><a href="mvc?logica=RemoveContatoLogic&id=${contato.id}">Remover</a></td>
				  <c:choose>
				        <c:when test="${not empty contato.email}">
				           <td><a href="mailto:${contato.email}">${contato.email}</a></td>
				        </c:when>
				        <c:otherwise>
				            E-mail n�o informado
				        </c:otherwise>
    				</c:choose>
 				<td>${contato.endereco}</td>
					<td><fmt:formatDate value="${contato.dataNascimento.time}"
        			pattern="dd/MM/yyyy" /></td>
 			</tr>
	     </c:forEach>
	     
	    </table>


												
