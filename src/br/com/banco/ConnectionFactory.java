package br.com.banco;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionFactory {
	public Connection getConnection() {
		Connection conn = null;
		try {
			InitialContext cxt = new InitialContext();
			DataSource ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/postgres");
			if (ds != null) {
				conn = ds.getConnection();

			}
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
		return (conn);
	}
}
