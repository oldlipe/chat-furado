package br.com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import br.com.banco.ConnectionFactory;

public class MessageDao {
	private Connection conexao;

	public MessageDao() {
		this.conexao = new ConnectionFactory().getConnection();
	}

	public void adiciona(Message message) {
		String sql = "insert into message " + "(username, email, message) " + "values (?,?,?)";

		try {
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setString(1, message.getUsername());
			stmt.setString(2, message.getEmail());
			stmt.setString(3, message.getMessage());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void exclui(Message message) {
		String sql = "delete from message " + "where id = ?";
		try {
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setLong(1, message.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Message> getLista() {
		try {
			PreparedStatement stmt = this.conexao.prepareStatement("select * from message");

			ResultSet rs = stmt.executeQuery();

			List<Message> messages = new ArrayList<Message>();

			while (rs.next()) {
				Message message = new Message();
				message.setId(rs.getLong("id"));
				message.setUsername(rs.getString("username"));
				message.setEmail(rs.getString("email"));
				message.setMessage(rs.getString("message"));
				messages.add(message);
			}
			rs.close();
			stmt.close();
			return messages;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
