package br.com.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.logica.ListaMessageController;

@WebServlet("/view")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
	
		
		ListaMessageController logica = new ListaMessageController();
		
        String pagina = null;
		try {
			pagina = logica.executa(request, response);
	        request.getRequestDispatcher(pagina).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
