package br.com.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.model.Message;
import br.com.model.MessageDao;

@WebServlet("/posta")
public class Adiciona extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Message message = new Message();

		message.setEmail(request.getParameter("email"));
		message.setMessage(request.getParameter("message"));
		message.setUsername(request.getParameter("username"));

		new MessageDao().adiciona(message);
		request.getRequestDispatcher("/view").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
