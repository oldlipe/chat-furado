package br.com.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.model.Message;
import br.com.model.MessageDao;

public class RemoveMessageController implements Logica {
	public String executa(HttpServletRequest req, HttpServletResponse res)
            throws Exception {

        long id = Long.parseLong(req.getParameter("id"));

        Message message = new Message();
        message.setId(id);

        new MessageDao().exclui(message);

        return "query?logica=ListaMessageController";
    }

}
