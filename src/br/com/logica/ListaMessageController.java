package br.com.logica;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.model.Message;
import br.com.model.MessageDao;

public class ListaMessageController implements Logica  {
    public String executa(HttpServletRequest req, HttpServletResponse res)
            throws Exception {
	
	  List<Message> listaPosts = new MessageDao().getLista();

      // Guarda a lista no request
      req.setAttribute("listaPosts", listaPosts);

      return "exibe.jsp";
    }

}
