package br.com.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.model.Message;
import br.com.model.MessageDao;

public class AdicionaMessageController {
	public String executa(HttpServletRequest req, HttpServletResponse res)
            throws Exception {	
	
	Message message = new Message();
    
    message.setEmail(req
    		.getParameter("email"));
    message.setMessage(req
    		.getParameter("message"));
    message.setUsername(req
    		.getParameter("username"));
    		
    new MessageDao().adiciona(message);
    
    return "mvc?logica=ListaContatoLogic";   
    
	}
}
